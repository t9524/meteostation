#include <DHT.h>
#include <DHT_U.h>

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>

#include <LiquidCrystal_I2C.h>

#include <DS1302.h>


#define BMP_SCK  (13)
#define BMP_MISO (12)
#define BMP_MOSI (11)
#define BMP_CS   (10)
#define DHTPIN_outside 9
#define DHTPIN_inside 8

#define CE_pin 4
#define I_O_pin 5
#define SCLK_pin 6

#define mode_of_work 1 // 1 -- отладка, 0 -- работа

byte last_second = 99;
unsigned long last_time1 = 0;
unsigned long last_time2 = 0;

DHT dht_outside(DHTPIN_outside, DHT22);
DHT dht_inside(DHTPIN_inside, DHT11);

Adafruit_BMP280 bmp(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK);
LiquidCrystal_I2C lcd(0x27, 20, 4); // присваиваем имя LCD для дисплея
DS1302 rtc(4, 5, 6);
Time t;

void print_dht_inside_t_h(){
    float h_inside = dht_inside.readHumidity();
    float t_inside = dht_inside.readTemperature();

    if (isnan(t_inside) || isnan(h_inside)) {
        if(mode_of_work)Serial.println("Failed to read from inside DHT");

            lcd.setCursor(3, 0);
            lcd.print(" ??? ");
            lcd.setCursor(3, 1);
            lcd.print(" ??? ");
    }
    else {
        if(mode_of_work){
            Serial.print("Humidity inside: ");
            Serial.print(h_inside);
            Serial.print(" %\t");
            Serial.print("Temperature inside: ");
            Serial.print(t_inside);
            Serial.println(" *C");
        }
    lcd.setCursor(3, 0);
    lcd.print("     ");
    lcd.setCursor(3, 0);
    lcd.print(t_inside);
    lcd.setCursor(3, 1);
    lcd.print("     ");
    lcd.setCursor(3, 1);
    lcd.print(h_inside);
    }
}
void print_dht_outside_t_h(){
    float h_outside = dht_outside.readHumidity();
    float t_outside = dht_outside.readTemperature();

    if (isnan(t_outside) || isnan(h_outside)) {
        if(mode_of_work)Serial.println("Failed to read from outside DHT");
            lcd.setCursor(14, 0);
            lcd.print(" ??? ");
            lcd.setCursor(14, 1);
            lcd.print(" ??? ");
    }
    else {
        if(mode_of_work){
            Serial.print("Humidity outside: ");
            Serial.print(h_outside);
            Serial.print(" %\t");
            Serial.print("Temperature outside: ");
            Serial.print(t_outside);
            Serial.println(" *C");
        }
        
    
    lcd.setCursor(14, 0);
    lcd.print("     ");
    lcd.setCursor(14, 0);
    lcd.print(t_outside);
    lcd.setCursor(14, 1);
    lcd.print("     ");
    lcd.setCursor(14, 1);
    lcd.print(h_outside);
    }
}
void print_time_and_date(){
    if(mode_of_work){
        Serial.print(rtc.getDOWStr());
        Serial.print(" ");
        Serial.print(rtc.getDateStr());
        Serial.print(" -- ");
        Serial.println(rtc.getTimeStr());
    }
    
    lcd.setCursor(6, 3);
    lcd.print(rtc.getTimeStr());
    lcd.setCursor(0, 2);
    lcd.print(rtc.getDateStr());
}
void print_barometer(){
    if(mode_of_work){
          Serial.print(F("Pressure = "));
          Serial.print(bmp.readPressure());
          Serial.println(" Pa");
          Serial.println();
    }
    float pressure = bmp.readPressure()/133.322;
    lcd.setCursor(13, 2);
    lcd.print(pressure, 2);
}


void setup() {
  rtc.halt(false);
  rtc.writeProtect(false);
  //rtc.setDOW(SUNDAY);        // Set Day-of-Week to FRIDAY
  //rtc.setTime(22, 51, 0);     // Set the time to 12:00:00 (24hr format)
  //rtc.setDate(23, 01, 2022);   // Set the date to August 6th, 2010
  
  Serial.begin(9600);

  dht_outside.begin();



  //////////////////////////
  
  unsigned status;

  status = bmp.begin();
  if (!status) {
      Serial.println("error");
          
    
    lcd.setCursor(0, 1);
    lcd.print("barometer not found!");
    delay(500);
    while (1) {
        if (status) break;
        delay(2000);
    }
  }
  
   bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
    lcd.begin();
    delay(1000);
    lcd.setCursor(0, 3);
    lcd.print("-----          -----");
    lcd.setCursor(0, 0);
    lcd.print("t: ");
    lcd.setCursor(11, 0);
    lcd.print("t: ");
    lcd.setCursor(11, 1);
    lcd.print("h: ");
    lcd.setCursor(0, 1);
    lcd.print("h: ");
    lcd.setCursor(8, 1);
    lcd.print("%");
    lcd.setCursor(19, 1);
    lcd.print("%");

        print_barometer();
        print_dht_inside_t_h();
        print_dht_outside_t_h();
        print_time_and_date();

}

void loop() {
  
    if(millis() - last_time1 >= 300000){
        last_time1 = millis();
        print_barometer();
        print_dht_inside_t_h();
        print_dht_outside_t_h();
    }
    if(millis() - last_time2 >= 200){
        last_time2 = millis();
        t = rtc.getTime();
        if(last_second != t.sec){
            last_second = t.sec;
            print_time_and_date();
        }
    }
}
